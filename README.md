## DirectGMA source filter

The `direct-gma` filter requires AMD's OpenCL SDK for the DirectGMA extension.

This plugin assumes the data stored on the FPGA to be in 16 bit unsigned integer
format.

### DirectGMA properties

* `width` of the output buffer
* `height` of the output buffer
* `number` of frames to produce.
* `num-pages` controls the number of pages to transmit per block.
* `time` specifies if timing information is supposed to be printed on stdout
