/*
 * Copyright (C) 2011-2015 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <string.h>
#include <CL/cl.h>
#include <CL/cl_ext.h>
#include <pcilib.h>
#include <pcilib/bar.h>
#include <pcilib/kmem.h>
#include "ufo-direct-gma-task.h"
#include "hf-interface.h"

/* this should actually come from the distributed pcitool sources */
#include "pciDriver.h"

typedef struct {
    cl_mem mem;
    cl_bus_address_amd addr;
} AddressableBuffer;

struct _UfoDirectGmaTaskPrivate {
    /* pcilib */
    pcilib_t             *pci;
    guint8               *bar;
    guint8                board_gen;
    uintptr_t             kdesc_bus;
    pcilib_kmem_handle_t *kdesc;
    volatile guint32     *desc;

    /* OpenCL */
    cl_context          context;
    gsize               size;
    gsize               padded;
    guint               current;
    AddressableBuffer   buffers[2];

    /* thread management */
    GAsyncQueue        *write_queue;
    GAsyncQueue        *read_queue;
    GThread            *thread;

    /* properties */
    gsize block_size;
    guint num_pages;
    guint num_frames;
    guint width;
    guint height;
    gboolean time;

    GTimer *timer;
};

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoDirectGmaTask, ufo_direct_gma_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init))

#define UFO_DIRECT_GMA_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_DIRECT_GMA_TASK, UfoDirectGmaTaskPrivate))

#define KMEM_DEFAULT_FLAGS      PCILIB_KMEM_FLAG_HARDWARE | \
                                PCILIB_KMEM_FLAG_PERSISTENT | \
                                PCILIB_KMEM_FLAG_EXCLUSIVE

#define KMEM_USE_RING           PCILIB_KMEM_USE(PCILIB_KMEM_USE_USER, 1)
#define KMEM_USE_DEFAULT        PCILIB_KMEM_USE(PCILIB_KMEM_USE_USER, 2)

#ifndef NDEBUG
#define WR32(addr, value) *(uint32_t *) (priv->bar + (addr)) = (value);\
        g_print ("WR32N %4x  <- %x\n", (addr), (unsigned) (value));
#define WR32_sleep(addr, value) *(uint32_t *) (priv->bar + (addr)) = (value); g_usleep (10);\
        g_print ("WR32S %4x  <- %x\n", (addr), (unsigned) (value));
#define WR64(addr, value) *(uint64_t *) (priv->bar + (addr)) = (value);\
        g_print ("WR64N %4x  <- %x\n", (addr), (unsigned) (value));
#define WR64_sleep(addr, value) *(uint64_t *) (priv->bar + (addr)) = (value); g_usleep (10);\
        g_print ("WR64S %4x  <- %x\n", (addr), (unsigned) (value));
#else
#define WR32(addr, value) *(uint32_t *) (priv->bar + (addr)) = (value);
#define WR32_sleep(addr, value) *(uint32_t *) (priv->bar + (addr)) = (value); g_usleep (10);
#define WR64(addr, value) *(uint64_t *) (priv->bar + (addr)) = (value);
#define WR64_sleep(addr, value) *(uint64_t *) (priv->bar + (addr)) = (value); g_usleep (10);
#endif

#define RD32(addr) (*(uint32_t *) (priv->bar + (addr)))
#define RD64(addr) (*(uint64_t *) (priv->bar + (addr)))

enum {
    PROP_0,
    PROP_NUM_PAGES,
    PROP_NUM_FRAMES,
    PROP_WIDTH,
    PROP_HEIGHT,
    PROP_TIME,
    N_PROPERTIES
};

static const size_t     TLP_SIZE            = 32;
static const uint32_t   PAGE_SIZE           = 4096;

static clEnqueueMakeBuffersResidentAMD_fn clEnqueueMakeBuffersResidentAMD = NULL;
static GParamSpec *properties[N_PROPERTIES] = { NULL, };

/* declaration should actually come from a distributed header file */
const pcilib_board_info_t *pcilib_get_board_info (pcilib_t *);


static gboolean
init_pcilib (UfoDirectGmaTaskPrivate *priv, GError **error)
{
    static const char *DEVICE = "/dev/fpga0";

    priv->pci = pcilib_open (DEVICE, "pci");

    if (priv->pci == NULL) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP,
                     "Could not open `%s'", DEVICE);
        return FALSE;
    }

    priv->bar = pcilib_map_bar (priv->pci, PCILIB_BAR0);

    if (priv->bar == NULL) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP, "Unable to map BAR");
        pcilib_close (priv->pci);
        return FALSE;
    }

    priv->board_gen = RD32 (0x18) & 0xF;
    priv->kdesc = pcilib_alloc_kernel_memory (priv->pci, PCILIB_KMEM_TYPE_CONSISTENT, 1, 128, 4096, KMEM_USE_RING, KMEM_DEFAULT_FLAGS);
    priv->kdesc_bus = pcilib_kmem_get_block_ba (priv->pci, priv->kdesc, 0);
    priv->desc = (uint32_t *) pcilib_kmem_get_block_ua (priv->pci, priv->kdesc, 0);

    memset ((uint32_t *) priv->desc, 0, 5 * sizeof (uint32_t));

    g_debug ("dgma: FPGA Firmware version 0x%X", RD32 (HF_REG_VERSION));

    return TRUE;
}

static cl_platform_id
get_direct_gma_platform (UfoResources *resources, GError **error)
{
    GList *devices;
    cl_device_id device;
    cl_platform_id platform;

    devices = ufo_resources_get_devices (resources);

    if (g_list_length (devices) == 0) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP,
                     "No devices found");
        return NULL;
    }

    if (g_list_length (devices) > 1)
        g_debug ("dgma: more than one device found, using the first one");

    device = g_list_nth_data (devices, 0);

    UFO_RESOURCES_CHECK_AND_SET (clGetDeviceInfo (device, CL_DEVICE_PLATFORM, sizeof (cl_platform_id), &platform, NULL), error);

    if (*error != NULL)
        return NULL;

    g_list_free (devices);
    return platform;
}

static gboolean
create_buffer (cl_context context, cl_command_queue queue, gsize size, AddressableBuffer *buffer, GError **error)
{
    cl_mem_flags flags;
    cl_event event;
    cl_int cl_error;
    guint32 pattern = 0xD00DF00D;

    flags = CL_MEM_BUS_ADDRESSABLE_AMD | CL_MEM_READ_WRITE;
    buffer->mem = clCreateBuffer (context, flags, size, NULL, &cl_error);
    UFO_RESOURCES_CHECK_AND_SET (cl_error, error);

    if (*error != NULL)
        return FALSE;

    cl_error = clEnqueueMakeBuffersResidentAMD (queue, 1, &buffer->mem, CL_TRUE, &buffer->addr, 0, NULL, NULL);
    UFO_RESOURCES_CHECK_AND_SET (cl_error, error);

    cl_error = clEnqueueFillBuffer (queue, buffer->mem, &pattern, 4, 0, size / 4, 0, NULL, &event);
    UFO_RESOURCES_CHECK_AND_SET (cl_error, error);
    clWaitForEvents (1, &event);
    clReleaseEvent (event);

    return TRUE;
}

static gboolean
init_opencl (UfoDirectGmaTaskPrivate *priv, UfoResources *resources, GError **error)
{
    cl_platform_id platform;
    cl_context context;
    cl_command_queue queue;
    GList *queues;

    platform = get_direct_gma_platform (resources, error);

    if (platform == NULL)
        return FALSE;

    clEnqueueMakeBuffersResidentAMD = clGetExtensionFunctionAddressForPlatform (platform, "clEnqueueMakeBuffersResidentAMD");

    if (clEnqueueMakeBuffersResidentAMD == NULL) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP,
                     "Unable to load clEnqueueMakeBuffersResidentAMD");
        return FALSE;
    }

    context = ufo_resources_get_context (resources);
    queues = ufo_resources_get_cmd_queues (resources);
    queue = g_list_nth_data (queues, 0);
    g_list_free (queues);

    priv->padded = priv->size + ((priv->size % priv->block_size) ? priv->block_size - priv->size % priv->block_size : 0);

    if (priv->padded / priv->block_size >= HF_MAX_NUM_PACKETS) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP,
                     "Too many blocks requested, try increasing `num-pages'.");
    }

    if (!create_buffer (context, queue, priv->padded, &priv->buffers[0], error))
        return FALSE;

    if (!create_buffer (context, queue, priv->padded, &priv->buffers[1], error))
        return FALSE;

    return TRUE;
}

static void
close_pcilib (UfoDirectGmaTaskPrivate *priv)
{
    pcilib_free_kernel_memory (priv->pci, priv->kdesc, KMEM_DEFAULT_FLAGS);
    pcilib_unmap_bar (priv->pci, PCILIB_BAR0, (void *) priv->bar);
    pcilib_close (priv->pci);
}

UfoNode *
ufo_direct_gma_task_new (void)
{
    return UFO_NODE (g_object_new (UFO_TYPE_DIRECT_GMA_TASK, NULL));
}

static void
ufo_direct_gma_task_setup (UfoTask *task,
                           UfoResources *resources,
                           GError **error)
{
    UfoDirectGmaTaskPrivate *priv;

    priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE (task);
    priv->size = priv->width * priv->height * sizeof (guint32);

    if (!init_pcilib (priv, error))
        return;

    if (!init_opencl (priv, resources, error))
        return;

    priv->current = 0;
}

static void
ufo_direct_gma_task_get_requisition (UfoTask *task,
                                     UfoBuffer **inputs,
                                     UfoRequisition *requisition)
{
    UfoDirectGmaTaskPrivate *priv;

    priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE (task);
    requisition->n_dims = 2;
    requisition->dims[0] = priv->width;
    requisition->dims[1] = priv->height;
}

static guint
ufo_direct_gma_task_get_num_inputs (UfoTask *task)
{
    return 0;
}

static guint
ufo_direct_gma_task_get_num_dimensions (UfoTask *task,
                                        guint input)
{
    return 0;
}

static UfoTaskMode
ufo_direct_gma_task_get_mode (UfoTask *task)
{
    return UFO_TASK_MODE_GENERATOR | UFO_TASK_MODE_GPU;
}

static void
busywait (uint32_t usec)
{
    struct timeval tv_start, tv_now;

    gettimeofday (&tv_start, NULL);

    while (1) {
        gettimeofday (&tv_now, NULL);

        if (((tv_now.tv_sec - tv_start.tv_sec) * 1000 * 1000 + (tv_now.tv_usec - tv_start.tv_usec)) >= usec)
            break;
    }
}

static gpointer
acquire_data (UfoDirectGmaTaskPrivate *priv)
{
    guint num_blocks;
    guint flag_index;
    gboolean dma_started;

#ifdef MEASURE_PERFORMANCE
    GTimer *timer;
    guint32 current;
    guint32 previous;
    guint32 finegrained;
#endif

    /* configure interconnect */
    WR32_sleep (HF_REG_INTERCONNECT,
                HF_INTERCONNECT_MASTER_DMA |
                HF_INTERCONNECT_DDR_TO_DMA |
                HF_INTERCONNECT_DDR_FROM_64);

    /* reset DMA */
    WR32_sleep (HF_REG_BASE, 1);
    WR32_sleep (HF_REG_BASE, 0);

    WR32_sleep (HF_REG_NUM_PACKETS, priv->block_size / (4 * TLP_SIZE));
    WR32_sleep (HF_REG_PACKET_LENGTH, TLP_SIZE);
    WR32_sleep (HF_REG_TIMER_THRESHOLD, 0x2000);

    WR32_sleep (HF_REG_UPDATE_THRESHOLD, 1);
    WR64_sleep (HF_REG_UPDATE_ADDRESS, priv->kdesc_bus);

    flag_index = 2;
    num_blocks = priv->padded / priv->block_size;

#ifdef MEASURE_PERFORMANCE
    timer = g_timer_new ();
    previous = 0;
#endif

    dma_started = FALSE;

    g_debug ("dgma: block_size=%zu padded=%zu num_blocks=%u", priv->block_size, priv->padded, num_blocks);

    /* verified with firmware version 100FC */

    for (guint i = 0; i < priv->num_frames; i++) {
        AddressableBuffer *buffer;
        guint64 addr;
        guint64 hardware_ptr;

        buffer = g_async_queue_pop (priv->write_queue);
        priv->desc[flag_index] = 0;
        hardware_ptr = 0;

        /* write addresses */
        addr = buffer->addr.surface_bus_address;

        for (guint i = 0; i < num_blocks; i++, addr += priv->block_size) {
            WR64 (HF_REG_DESCRIPTOR_ADDRESS, addr);
        }

        if (!dma_started) {
            WR32_sleep (HF_REG_DMA, HF_DMA_START);
            dma_started = TRUE;
        }

#ifdef MEASURE_PERFORMANCE
        WR32_sleep (HF_REG_DEBUG_REQUESTER_RESET, 1);
        WR32_sleep (HF_REG_DEBUG_REQUESTER_RESET, 0);
        WR32_sleep (HF_REG_DEBUG_COMPLETER_RESET, 1);
        WR32_sleep (HF_REG_DEBUG_COMPLETER_RESET, 0);

        g_timer_start (timer);
#endif
        /* go back */
        WR32 (HF_REG_CONTROL,
              HF_CONTROL_ENABLE_READ |
              HF_CONTROL_ENABLE_MULTI_READ |
              HF_CONTROL_SOURCE_RX_FIFO);

        busywait (1);

        WR32 (HF_REG_CONTROL,
              HF_CONTROL_ENABLE_READ |
              HF_CONTROL_SOURCE_RX_FIFO);

        busywait (1);

        /* wait for data transmission */
        addr -= priv->block_size;

        do {
            hardware_ptr = priv->desc[flag_index];
        }
        while ((hardware_ptr != (addr & 0xFFFFFFFF)) || !(priv->desc[1] & 1));

#ifdef MEASURE_PERFORMANCE
        g_timer_stop (timer);
        current = RD32 (HF_REG_PERF_COUNTER);
        finegrained = RD32 (HF_REG_MULTIREAD_COUNTER);

        g_debug ("dgma: perf=%3.2f us, fpga=%3.2f us, wall=%3.2f us",
                 (current - previous) * 256 * 4.0 / 1000.0,
                 finegrained * 4 / 1000.0,
                 g_timer_elapsed (timer, NULL) * 1000.0 * 1000.0);

        previous = current;
#endif

        /* release filled buffer */
        g_async_queue_push (priv->read_queue, buffer);
    }

    WR32_sleep (HF_REG_DMA, HF_DMA_STOP);

#ifdef MEASURE_PERFORMANCE
    g_timer_destroy (timer);
#endif
    return NULL;
}

static gboolean
ufo_direct_gma_task_generate (UfoTask *task,
                              UfoBuffer *output,
                              UfoRequisition *requisition)
{
    UfoDirectGmaTaskPrivate *priv;
    UfoGpuNode *node;
    cl_mem dst_buffer;
    cl_command_queue queue;
    cl_event event;
    AddressableBuffer *buffer;

    priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE (task);

    if (priv->time) {
        if (priv->timer == NULL)
            priv->timer = g_timer_new ();
        else
            g_timer_continue (priv->timer);
    }

    if (priv->current == priv->num_frames) {
        g_thread_join (priv->thread);
        g_thread_unref (priv->thread);

        if (priv->time) {
            gdouble elapsed;

            g_timer_stop (priv->timer);
            elapsed = g_timer_elapsed (priv->timer, NULL);
            g_print ("ufo_direct_gma_task_generate: %3.5f seconds [%3.5f MB/s]\n",
                     elapsed, priv->num_frames * priv->size / elapsed / 1024. / 1024.);
            g_timer_destroy (priv->timer);
        }

        g_debug ("dgma: acquisition thread finished");
        return FALSE;
    }

    if (priv->current == 0) {
        /* push all buffers to write queue before we start the thread*/
        g_async_queue_push (priv->write_queue, &priv->buffers[0]);
        g_async_queue_push (priv->write_queue, &priv->buffers[1]);

        priv->thread = g_thread_new ("acquire", (GThreadFunc) acquire_data, priv);
        g_debug ("dgma: started acquistion thread");
    }

    node = UFO_GPU_NODE (ufo_task_node_get_proc_node (UFO_TASK_NODE (task)));
    queue = ufo_gpu_node_get_cmd_queue (node);
    dst_buffer = ufo_buffer_get_device_array (output, queue);
    buffer = g_async_queue_pop (priv->read_queue);

    /* Copy to final output buffer */
    UFO_RESOURCES_CHECK_CLERR (clEnqueueCopyBuffer (queue, buffer->mem, dst_buffer, 0, 0, priv->size, 0, NULL, &event));
    UFO_RESOURCES_CHECK_CLERR (clWaitForEvents (1, &event));
    UFO_RESOURCES_CHECK_CLERR (clReleaseEvent (event));

    g_async_queue_push (priv->write_queue, buffer);
    priv->current++;

    if (priv->time)
        g_timer_stop (priv->timer);

    return TRUE;
}

static void
ufo_direct_gma_task_set_property (GObject *object,
                                  guint property_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
    UfoDirectGmaTaskPrivate *priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_NUM_PAGES:
            priv->num_pages = g_value_get_uint (value);
            priv->block_size = priv->num_pages * PAGE_SIZE;
            break;
        case PROP_NUM_FRAMES:
            priv->num_frames = g_value_get_uint (value);
            break;
        case PROP_WIDTH:
            priv->width = g_value_get_uint (value);
            break;
        case PROP_HEIGHT:
            priv->height = g_value_get_uint (value);
            break;
        case PROP_TIME:
            priv->time = g_value_get_boolean (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_direct_gma_task_get_property (GObject *object,
                                  guint property_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
    UfoDirectGmaTaskPrivate *priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_NUM_PAGES:
            g_value_set_uint (value, priv->num_pages);
            break;
        case PROP_NUM_FRAMES:
            g_value_set_uint (value, priv->num_frames);
            break;
        case PROP_WIDTH:
            g_value_set_uint (value, priv->width);
            break;
        case PROP_HEIGHT:
            g_value_set_uint (value, priv->height);
            break;
        case PROP_TIME:
            g_value_set_boolean (value, priv->time);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_direct_gma_task_finalize (GObject *object)
{
    UfoDirectGmaTaskPrivate *priv;

    priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE (object);

    g_async_queue_unref (priv->write_queue);
    g_async_queue_unref (priv->read_queue);
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->buffers[0].mem));
    UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->buffers[1].mem));

    close_pcilib (priv);

    G_OBJECT_CLASS (ufo_direct_gma_task_parent_class)->finalize (object);
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
    iface->setup = ufo_direct_gma_task_setup;
    iface->get_num_inputs = ufo_direct_gma_task_get_num_inputs;
    iface->get_num_dimensions = ufo_direct_gma_task_get_num_dimensions;
    iface->get_mode = ufo_direct_gma_task_get_mode;
    iface->get_requisition = ufo_direct_gma_task_get_requisition;
    iface->generate = ufo_direct_gma_task_generate;
}

static void
ufo_direct_gma_task_class_init (UfoDirectGmaTaskClass *klass)
{
    GObjectClass *oclass = G_OBJECT_CLASS (klass);

    oclass->set_property = ufo_direct_gma_task_set_property;
    oclass->get_property = ufo_direct_gma_task_get_property;
    oclass->finalize = ufo_direct_gma_task_finalize;

    properties[PROP_NUM_PAGES] =
        g_param_spec_uint ("num-pages",
                           "Number of pages",
                           "Number of pages",
                           1, 4096, 16,
                           G_PARAM_READWRITE);

    properties[PROP_NUM_FRAMES] =
        g_param_spec_uint ("number",
                           "Number of frames",
                           "Number of frames",
                           1, G_MAXUINT, 1,
                           G_PARAM_READWRITE);

    properties[PROP_WIDTH] =
        g_param_spec_uint ("width",
                           "Frame width",
                           "Frame width",
                           1, G_MAXUINT, 2048,
                           G_PARAM_READWRITE);

    properties[PROP_HEIGHT] =
        g_param_spec_uint ("height",
                           "Frame height",
                           "Frame height",
                           1, G_MAXUINT, 2048,
                           G_PARAM_READWRITE);

    properties[PROP_TIME] =
        g_param_spec_boolean ("time",
                              "Whether to measure and output generate time",
                              "Whether to measure and output generate time",
                              FALSE, G_PARAM_READWRITE);

    for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
        g_object_class_install_property (oclass, i, properties[i]);

    g_type_class_add_private (oclass, sizeof(UfoDirectGmaTaskPrivate));
}

static void
ufo_direct_gma_task_init(UfoDirectGmaTask *self)
{
    self->priv = UFO_DIRECT_GMA_TASK_GET_PRIVATE(self);
    self->priv->width = 2048;
    self->priv->height = 2048;
    self->priv->num_frames = 1;
    self->priv->num_pages = 16;
    self->priv->block_size = self->priv->num_pages * PAGE_SIZE;
    self->priv->time = FALSE;
    self->priv->timer = NULL;
    self->priv->write_queue = g_async_queue_new ();
    self->priv->read_queue = g_async_queue_new ();
}
