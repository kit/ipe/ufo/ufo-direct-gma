#ifndef HF_INTERFACE_H
#define HF_INTERFACE_H

/* internal registers */

#define HF_REG_BASE                     0x00
#define HF_REG_DMA                      0x04
#define HF_REG_NUM_PACKETS              0x10
#define HF_REG_PERF_COUNTER             0x28
#define HF_REG_MULTIREAD_COUNTER        0x30
#define HF_REG_PACKET_LENGTH            0x0C
#define HF_REG_DESCRIPTOR_ADDRESS       0x50
#define HF_REG_UPDATE_ADDRESS           0x58
#define HF_REG_UPDATE_THRESHOLD         0x60
#define HF_REG_TIMER_THRESHOLD          0x64

/* internal register masks and values */

#define HF_BASE_RESET                   0x00000001
#define HF_BASE_VERSION                 0x0000FF00
#define HF_BASE_DATAPATH_WIDTH          0x000F0000
#define HF_BASE_FPGA_FAMILY             0xFF000000
#define HF_DMA_START                    0x1
#define HF_DMA_STOP                     0x0


/* user registers */

#define HF_REG_DCG                      0x9000
#define HF_REG_DCG_FIXED_PATTERN        0x9004
#define HF_REG_DCG_FIXED_PATTERN_1ST    0x9008
#define HF_REG_VERSION                  0x9030
#define HF_REG_CONTROL                  0x9040
#define HF_REG_LATENCY                  0x9044
#define HF_REG_INTERCONNECT             0x9048
#define HF_REG_DFG_NUM_ROWS             0x9168
#define HF_REG_DFG_NUM_FRAMES           0x9170
#define HF_REG_DEBUG_REQUESTER_RESET    0x9344
#define HF_REG_DEBUG_COMPLETER_RESET    0x93A4

/* user register masks and values */

#define HF_DCG_START                    0x00000001
#define HF_DCG_RESET                    0x00000010
#define HF_DCG_ENABLE_FIXED_PATTERN     0x00000100
#define HF_CONTROL_RESET                0x00000004
#define HF_CONTROL_SOFT_RESET           0x00000008
#define HF_CONTROL_ENABLE_READ          0x00000400
#define HF_CONTROL_ENABLE_MULTI_READ    0x00000800
#define HF_CONTROL_GLOBAL_COUNTER_RESET 0x00008000
#define HF_CONTROL_SOURCE_MASK          0x000F0000
#define HF_CONTROL_SOURCE_DCG           0x00000000
#define HF_CONTROL_SOURCE_RX_FIFO       0x00010000
#define HF_CONTROL_SOURCE_DFG           0x00020000
#define HF_CONTROL_SOURCE_DCG_NO_DDR    0x00030000

#define HF_MAX_NUM_PACKETS              8192
#define HF_INTERCONNECT_MASTER_DMA      0x2
#define HF_INTERCONNECT_SLAVE_DMA       0x1
#define HF_INTERCONNECT_DDR_TO_DMA      0x60
#define HF_INTERCONNECT_DDR_FROM_64     0x200

#endif
